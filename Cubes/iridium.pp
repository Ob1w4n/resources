##################################################################
# File per la costruzione di un cluster IRIDIUM del gruppo CUBES #
##################################################################

class standard_things {
   
    exec { "APT Sources Conditioning": 
		path => '/usr/bin:/bin',
		command => "sed -i -e 's/main/main non-free/g' /etc/apt/sources.list", }

#	file { "PBIS Repo":
#		path => "/etc/apt/sources.list.d/pbiso.list",
#		ensure => present,
#		owner => root,
#		group => root,
#		mode => "0644",
#		content => "deb https://repo.pbis.beyondtrust.com/apt pbiso main\n#deb https://repo.pbis.beyondtrust.com/apt pbiso non-free",	
#	}
	
#	package { "gnupg": ensure=>installed,}
#	exec { 'WGET OpenPBS Key': 
#		command=>'wget -O - http://repo.pbis.beyondtrust.com/apt/RPM-GPG-KEY-pbis| apt-key add -',
#		cwd=>"/root",
#		user=>root,
#		path=>"/usr/bin",
#	}
#	exec { 'WGET OpenPBS Repo':
#		command=>'wget -O /etc/apt/sources.list.d/pbiso.list http://repo.pbis.beyondtrust.com/apt/pbiso.list',
#		cwd=>"/root",
#		path=>"/usr/bin",
#	}
#	exec { 'FULL UPDATE': command=>'/usr/bin/apt-get clean && /usr/bin/apt-get update && /usr/bin/apt-get dist-upgrade -qy', }
#	package { "pbis-open": ensure=>installed, }
#	exec { "Set PBIS groups": command =>'/opt/pbis/bin/config RequireMembershipOf "RICERCA\\domain^admins" "RICERCA\\CUBES_bicocca" "RICERCA\\CUBES_ricerca" "BICOCCA\\domain^users"', }
#	service { "lwsmd": enable=>false, }
}


class iridium_common {

	file { "Storage credentials": path=>'/root/credentials', owner=>root, group=>users, mode=>'0600', content=>'user=cubes \n domain= \n password=AlberodiNatal3! '}
	file { "Homes mount point": path=>'/home/isilon', ensure=>direcotry, owner=>root, group=>users, mode='0777', }
	exec { "Adding mount point": path=>'/usr/bin:/bin',
		command=>'sed -i -e "$ a isilon\.unimib\.it:/cubes	/home/isilon	nfs	defaults,soft,credentials=/root/isilon	0	0" /etc/fstab',
		unless=>'grep isilon /etc/fstab',
	}

}

class utenza {

        ssh_authorized_key { 'Chiave per MPI':
                name => 'mpi@nanocospha',
                ensure => present,
                key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDbBoNpfRGIiY19x2qulUBDYSG9YC3QVByf3ZHTUfuVpJa2C/lnFA3n8bYNBWaJet1RpXDpZwlc0TOGkN8DuzBdUJ4XBu9//c37wFydFHYH6WzaeeLGLouHI0fdnsHsif3Fcbo4GByz24Y4kflDgKXd4AvUyqxjzgncKGrzDO3qeBUwzAh1eL4W0a6W4+BjsU+Nccxe1Fl6+3goI8G4ftU3PJTYo2GWCErdbQiLkOd+Aot18Arhm5EnxuvdSkedVnSu9HhW6fXWGnsd2GEHjjy7fIi/VJdMOo7A7166rKx8q8Tq7xXqQRpPDikU9LRRKilji+mdsSZlTD7+xtV26835',
                type => 'ssh-rsa',
                user => root,
        }

        file { "Private Key": path => "/root/.ssh/id_rsa", ensure => present, owner => root, group => root, mode => "0600",
                source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Cubes/id_rsa", }

        file { "Public Key": path => "/root/.ssh/id_rsa.pub", ensure => present, owner => root, group => root, mode => "0644",
                source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Cubes/id_rsa.pub", }

	user { "Sistemista CR":
		name=>'sistemista',
		ensure=>'present',
		home=>'/home/isilon/sistemista',
		managehome=>true,
		gid=>users,
		groups=>['sudo'],
		password=>'',
		shell=>'/bin/bash',
		purge_ssh_keys=>false,
	}


}


#node default {
node /iridium[0-9]+$/ {
	include standard_things
	package { "build-essential": ensure=>installed, }
	package { "cifs-utils": ensure=>installed, }
	package { "gcc": ensure=>installed, }
	package { "gfortran": ensure=>installed, }
	package { "gnuplot": ensure=>installed, }
	package { "htop": ensure=>installed, }
	package { "mc": ensure=>installed, }
	package { "nfs-common": ensure=>installed, }
	package { "p7zip": ensure=>installed, }
	package { "rsync": ensure=>installed, }
	package { "vim": ensure=>installed, }
	exec { "PROMPT": 
		command=>'/usr/bin/sed -i -e \'7i PS1="\\[\\033]0;\\u@\\h:\\w\\007\\]\\[\\033[01;31m\\]\\h\\[\\033[01;34m\\] \\W \\$\\[\\033[00m\\] "\' /root/.bashrc',
		path=>"/usr/bin:/bin",
	}

}
