#############################################################
# File per la costruzione di un cluster SLURM ai fini delle #
# esigenze del gruppo di ricerca sulla modellizzazione di   #
# crescita di materiali semiconduttori. Abbreviato in MOSEG #
#############################################################

class standard_things {
    if ($::lsbdistid == 'Ubuntu' and $::lsbdistrelease == '18.04') {
    	exec { "APT Sources Conditioning": command => "/bin/sed -i -e 's/main/main non-free/g' /etc/apt/sources.list", }
    } else {
        exec { "APT Sources Conditioning": command => "/usr/bin/sed -i -e 's/main/main non-free/g' /etc/apt/sources.list", }
    }
    
#	file { "APT Sources":
#		path => "/etc/apt/sources.list",
#		ensure => present,
#		content => "\ndeb http://mirror.switch.ch/ftp/mirror/debian/ buster main non-free
#		deb-src http://mirror.switch.ch/ftp/mirror/debian/ buster main non-free
#	
#		deb http://security.debian.org/ buster/updates main
#		deb-src http://security.debian.org/ buster/updates main
#
#		deb http://mirror.switch.ch/ftp/mirror/debian/ buster-updates main non-free
#		deb-src http://mirror.switch.ch/ftp/mirror/debian/ buster-updates main non-free\n",
#	}

#	file { "PBIS Repo":
#		path => "/etc/apt/sources.list.d/pbiso.list",
#		ensure => present,
#		owner => root,
#		group => root,
#		mode => "0644",
#		content => "deb https://repo.pbis.beyondtrust.com/apt pbiso main\n#deb https://repo.pbis.beyondtrust.com/apt pbise non-free",	
#	}
	
	package { "gnupg": ensure=>installed,}
	exec { 'WGET OpenPBS Key': 
		command=>'wget -O - http://repo.pbis.beyondtrust.com/apt/RPM-GPG-KEY-pbis| apt-key add -',
		cwd=>"/root",
		user=>root,
		path=>"/usr/bin:/bin",
	}
	exec { 'WGET OpenPBS Repo':
		command=>'wget -O /etc/apt/sources.list.d/pbiso.list http://repo.pbis.beyondtrust.com/apt/pbiso.list',
		cwd=>"/root",
		path=>"/usr/bin",
	}
	exec { 'FULL UPDATE': command=>'/usr/bin/apt-get clean && /usr/bin/apt-get update && /usr/bin/apt-get dist-upgrade -qy', }
	package { "pbis-open": ensure=>installed, }
	exec { "Set PBIS groups": command =>'/opt/pbis/bin/config RequireMembershipOf "RICERCA\\domain^admins" "RICERCA\\sige_bicocca" "RICERCA\\sige_ricerca" "BICOCCA\\domain^users"', }
	exec { "Set PBIS shell": command =>'/opt/pbis/bin/config LoginShellTemplate "/bin/bash"', }
	exec { "Set PBIS home dir": command =>'/opt/pbis/bin/config HomeDirTemplate "%H/%D/%U"', }
	exec { "Conditioning Skel BASHRC": 
		path => '/usr/bin:/bin',
		command => 'sed -i -e "94 i\alias dir=\'ls -halFv\'" -e "s/#force_color/force_color/g" /etc/skel/.bashrc', }	
}


class slurm_cluster {
	package { "slurm-wlm": ensure=>installed, }

	file { "cgroup.conf":
		path => "/etc/slurm-llnl/cgroup.conf",
		ensure => present,
		owner => root,
		group => root,
		mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Moseg/cgroup.conf",
	}

	file { "slurm.conf":
		path => "/etc/slurm-llnl/slurm.conf",
		ensure => present,
		owner => root,
		group => root,
		mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Moseg/slurm.conf",
	}

	file { "munge.key":
		path => "/etc/munge/munge.key",
		ensure => present,
		owner => munge,
		group => munge,
		mode => "0400",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Moseg/munge.key",
	}
	
	file { "/var/run/slurm-llnl/":
	    path => "/var/run/slurm-llnl/",
	    ensure => directory,
	    owner => slurm,
	    group => slurm,
	    mode => "0755",
	}

	service { "munge": ensure=>running, }
	
}



class moseg_Debian9 {
	package { "libpetsc3.7.5": ensure=>installed, }
	package { "libsuitesparseconfig4": ensure=>installed, }
	package { "libpython3.5-dev": ensure=>installed, }
	package { "libscotchparmetis-dev": ensure=>installed, }
}

class moseg_Debian10 {
	package { "libsuitesparseconfig5": ensure=>installed, }
	package { "libpython3.7-dev": ensure=>installed, }
	package { "libparmetis-dev": ensure=>installed, }
}



node moseg-master {
	include standard_things
	package { "nfs-kernel-server": ensure=> installed, }
	package { "sendmail": ensure => installed, }
#   package { "xserver-xorg": ensure => installed, }
	package { "sview": ensure => installed, }
	
	include slurm_cluster

	file { "/var/spool/slurmctld/":
	    path => "/var/spool/slurmctld/",
	    ensure => directory,
	    owner => slurm,
	    group => slurm,
	    mode => "0755",
	}
	
	file { "/var/log/slurm_jobcomp.log":
	    path => "/var/log/slurm_jobcomp.log",
	    ensure => present,
	    owner => slurm,
	    group => slurm,
	    mode => "0644",
	}

	service { "slurmctld": ensure=>running, }

	file { "Script distribConf.sh":
			path => "/etc/slurm-llnl/distribConf.sh",
			ensure => present,
			owner => root,
			group => root,
			mode => "0755",
			source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Moseg/distribConf.sh"
	}
	
}



#node default {
node /moseg[0-9]+$/ {
	include standard_things

	package { "bison": ensure=>installed, }
    package { "build-essential": ensure=>installed, }
	package { "cmake-curses-gui": ensure=>installed, }
	package { "cmake": ensure=>installed, }
	package { "curl": ensure=>installed, }
	package { "flex": ensure=>installed, }
	package { "gcc": ensure=>installed, }
	package { "gfortran": ensure=>installed, }
	package { "git": ensure=>installed, }
	package { "gnuplot": ensure=>installed, }
	package { "libblas3": ensure=>installed, }
	package { "libblas-dev": ensure=>installed, }
	package { "liblapack-dev": ensure=>installed, }	
	package { "libboost-dev": ensure=>installed, }
	package { "libboost-all-dev": ensure=>installed, }
	package { "libfftw3-dev": ensure=>installed, }
	package { "libgsl-dev": ensure=>installed, }
	package { "liblapack3": ensure=>installed, }	
	package { "libmetis-dev": ensure=>installed, }
	package { "libopenmpi-dev": ensure=>installed, }	
#	package { "libparmetis-dev": ensure=>installed, }
	package { "libpython2.7": ensure=>installed, }
#	package { "libpython3.7-dev": ensure=>installed, }
#	package { "libsuitesparseconfig5": ensure=>installed, }
	package { "libumfpack5": ensure=>installed, }
#	package { "libwebkitgtk-3.0-0": ensure=>installed, }
	package { "openmpi-bin": ensure=>installed, }
	package { "paraview": ensure=>installed, }	
	package { "petsc-dev": ensure=>installed, }
	package { "pkgconf": ensure=>installed, }
	package { "python-vtk6": ensure=>installed, }
	package { "rsync": ensure=>installed, }
	package { "nfs-common": ensure=>installed, }
	package { "vim": ensure=>installed, }	
	package { "vtk6": ensure=>installed, }

	notify { 'Distro': message => "Found machine is $::lsbdistid $::lsbdistrelease",}
    if ($::lsbdistid == 'Ubuntu' and $::lsbdistrelease == '18.04') {
        include moseg_Debian9
    } else {
        include moseg_Debian10
    }

	include slurm_cluster
	service { "slurmd": ensure=>running, }
}
