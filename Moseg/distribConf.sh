#!/bin/sh

#pdsh -R ssh -w moseg[01-03] "systemctl stop slurmd"
#pdsh -R ssh -w moseg[01-03] "rm /etc/slurm-llnl/*.conf"

scp /etc/slurm-llnl/*.conf moseg01:/etc/slurm-llnl
scp /etc/slurm-llnl/*.conf moseg02:/etc/slurm-llnl
scp /etc/slurm-llnl/*.conf moseg03:/etc/slurm-llnl
scp /etc/slurm-llnl/*.conf sige01:/etc/slurm-llnl
scp /etc/slurm-llnl/*.conf sige02:/etc/slurm-llnl
scp /etc/slurm-llnl/*.conf sige03:/etc/slurm-llnl
scp /etc/slurm-llnl/*.conf sige04:/etc/slurm-llnl

#sleep 3s

#pdsh -R ssh -w moseg[01-03] "systemctl restart slurmd"
#pdsh -R ssh -w moseg[01-03] "systemctl status slurmd"

scontrol reconfigure

echo -e '\n\n\n'
tail -f /var/log/slurm-llnl/slurmctld.log
