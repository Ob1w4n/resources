##########################################################################
# File per l'installazione del window manager MATE e remotizzazione X2Go #
##########################################################################

node default {
	exec { "APT Sources Conditioning":
        command => "sed -i -e 's/main/main non-free/g' /etc/apt/sources.list",
        path => "/usr/bin:/bin",
    }

    exec { "APT Get Update":
        command => "apt-get update",
        path => "/usr/bin",
    }

    package { "unattended-upgrades": ensure => purged, }
    package { "ubuntu-mate-lightdm-theme": ensure => installed, }
 	package { "ubuntu-mate-desktop": ensure => installed, }
	package { "x2goserver": ensure => installed, }

    exec { "Cleanup":
        command => "apt-get autoremove --purge -qy",
        path => "/usr/bin",
    }

    exec { "Disable GDM3":
        command => "rm /etc/systemd/system/display-manager.service && ln -s /lib/systemd/system/lightdm.service /etc/systemd/system/display-manager.service",
        path => "/bin",
    }

    exec { "Replace X session":
        command => "rm /etc/alternatives/x-session-manager && ln -s /usr/bin/mate-session /etc/alternatives/x-session-manager",
        path => "/bin",
    }

    exec { "Reboot":
        command => "reboot",
        path => "/sbin",
    }
}
