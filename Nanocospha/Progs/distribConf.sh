#!/bin/sh

#pdsh -R ssh -w moseg[01-03] "systemctl stop slurmd"
#pdsh -R ssh -w moseg[01-03] "rm /etc/slurm-llnl/*.conf"

scp /etc/slurm/*.conf cn01:/etc/slurm
scp /etc/slurm/*.conf cn02:/etc/slurm
scp /etc/slurm/*.conf cn03:/etc/slurm
scp /etc/slurm/*.conf cn04:/etc/slurm
scp /etc/slurm/*.conf gpu01:/etc/slurm
#sleep 3s

#pdsh -R ssh -w moseg[01-03] "systemctl restart slurmd"
#pdsh -R ssh -w moseg[01-03] "systemctl status slurmd"

scontrol reconfigure

echo -e '\n\n\n'
tail -f /var/log/slurm/slurmctld.log
