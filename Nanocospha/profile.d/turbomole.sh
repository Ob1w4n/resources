#TURBOMOLE
#SMP signle node
#MPI multi node
export TURBODIR=/usr/local/TURBOMOLE-7.4.1
export TURBOMOLE_SYSNAME=em64t-unknown-linux-gnu_smp
export TURBOARCH=em64t-unknown-linux-gnu
export LD_LIBRARY_PATH=/usr/local/TURBOMOLE-7.4.1/libso/em64t-unknown-linux-gnu_smp
export PARA_ARCH=SMP
export PATH=$PATH:$TURBODIR/scripts
export PATH=$PATH:$TURBODIR/bin/`sysname`
export PARNODES=2
ulimit -s unlimited
