class standard_things {
	exec { 'Conditioning Skel BASHRC':
		path => '/usr/bin:/bin',
		command =>'sed -i -e "94 i\alias dir=\'ls -halFv --color\'" -e "s/#force_color/force_color/g" /etc/skel/.bashrc',
	}
	package { "bind9-utils": ensure=>installed, }
	package { "htop": ensure=>installed, }
	package { "man": ensure=>installed, }
	package { "mc": ensure=>installed, }
	package { "manpages": ensure=>installed, } 
	package { "mlocate": ensure=>installed, }                
	package { "nload": ensure=>installed, }
	package { "nfs-common": ensure=>installed, }
	package { "ntp": ensure=>installed, }
	package { "openssh-server": ensure=>installed, }
	package { "p7zip-full": ensure=>installed, }                
	package { "sudo": ensure=>installed, }
	package { "vim": ensure=>installed, }
	package { "wget": ensure=>installed, }
	package { "zip": ensure=>installed, }
	package { "inetutils-traceroute": ensure=>installed, }
}

class utenza {
	user { 'sistemista':
    		name => sistemista,
	        ensure => present,
        	home => '/home/sistemista',
	        managehome => true,
        	gid => users,
	        groups => ['sudo'],
        	password => '$1$.1P2.PTZ$Zzlv8Ye2pBaRMxhJ94bUG.',
	        shell => '/bin/bash',
        	purge_ssh_keys => false,
	}

	file { "Example molecule": path => "/home/sistemista/7s1s.pdb", ensure => present, owner => sistemista, group => users, mode => "0600",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/7s1s.pdb",
	}
		
	user { 'jacopo':
	        name => jacopo,
    		ensure => present,
	        home => '/home/storage01/jacopo',
        	managehome => true,
	        gid => users,
        	groups => ['sudo'],
	        password => '$1$hM/yMhOx$eNKSJUy5co3zGi/dlUNZi1',
        	shell => '/bin/bash',
	        purge_ssh_keys => false,
	}
	
user { 'federica':
	        name => farrigon,
    		ensure => present,
	        home => '/home/storage01/farrigon',
        	managehome => true,
	        gid => users,
        	groups => ['users'],
	        password => '$1$7NHsurcK$hdgrEhiG80mtdK.lnr0/r/',
        	shell => '/bin/bash',
	        purge_ssh_keys => false,
	}

	user { 'giu':
	        name => gzampell,
    		ensure => present,
	        home => '/home/storage01/gzampell',
        	managehome => true,
	        gid => users,
        	groups => ['users'],
	        password => '$1$UScuyHaq$NwkTq1IfJ4yJqYeSH40tV0',
        	shell => '/bin/bash',
	        purge_ssh_keys => false,
	}

	user { 'bert':
	        name => lbertini,
    		ensure => present,
	        home => '/home/storage01/lbertini',
        	managehome => true,
	        gid => users,
        	groups => ['users'],
	        password => '$1$Abl746lh$wcjy3xoRG1gJ/Nvs7X/rE0',
        	shell => '/bin/bash',
	        purge_ssh_keys => false,
	}

	user { 'giuseppesilvestri':
	        name => gsilvest,
    		ensure => present,
	        home => '/home/storage01/gsilvest',
        	managehome => true,
	        gid => users,
        	groups => ['users'],
	        password => '$1$PtHpU4zK$0bLcOezr7jl.QtxKzPnQf0',
        	shell => '/bin/bash',
	        purge_ssh_keys => false,
	}

	ssh_authorized_key { 'Chiave per MPI':
		name => 'mpi@nanocospha',
		ensure => present,
		key => 'AAAAB3NzaC1yc2EAAAADAQABAAABgQDinKOGgtJPeiJcpmdrMzvuL3i86h7AgSxMliAP6hExDFlcc28dvvbxs4NCXr/+Xovz5wJEYgFbjNrnJlHpQJybBj+twv7WVgiTwCF0a5Rwo3rdmQJ77Y75/VRZOPL9A4NWrT3IuAH+rrW21IlEIbql6tSUuJQNzTsJBoboDstNF/nt5lAbVOZYIahmyJyjdgFOSAGTGK3NHhck840Mkj/fAeS4q6KUDBhP7GvGWXf9XNgoTwr00Or43o6McEffMBmwah1G+CT1iWDQt8b+8JONIRloN5kyEDpENStlUaD9AbWvpza5j5VvKerxXdh6jZwHaOz7nOSeWQo3wUBrrW2LvyG2sH0J02vyVgdeN2NACmAoHLEco4JKYfc1J7xn/tfq2o2YggXpHca00j5TtWz1iHVkaN+mrt2Iyrq9jAf5Elvc+RtUvWigiCNpfsgH9wPjFo/Q76L6jUIXVi/JNpqK3Gez0GpCyI/2KycjPsI3NnwGigCMQ0cllYLL+9X57v0=',
		type => 'ssh-rsa',
		user => root,
	}

	file { "Private Key": path => "/root/.ssh/id_rsa", ensure => present, owner => root, group => root, mode => "0600",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/id_rsa", }

	file { "Public Key": path => "/root/.ssh/id_rsa.pub", ensure => present, owner => root, group => root, mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/id_rsa.pub", }


}

class cospa_common {
	package { "aria2": ensure=>installed, }
	package { "ca-certificates": ensure=>installed, }
	package { "curl": ensure=>installed, }
	package { "ifenslave": ensure=>installed, }
	package { "gcc": ensure=>installed, }
	package { "g++": ensure=>installed, }
	package { "libscalapack-mpi-dev": ensure=>installed, }
	package { "make": ensure=>installed, }
	package { "openbabel": ensure=>installed, }
	package { "build-essential": ensure=>installed, }
	package { "cmake": ensure=>installed, }
	package { "gfortran": ensure=>installed, }
	package { "git": ensure=>installed, }
	package { "gnupg": ensure=>installed, }
	package { "grace": ensure=>installed, }
	package { "gv": ensure=>installed, }
	package { "libblas-dev": ensure=>installed, }
	package { "libgconf-2-4": ensure=>installed, }
	package { "libgl1-mesa-glx": ensure=>installed, }
	package { "libgl1-mesa-dev": ensure=>installed, }
	package { "libglu1-mesa-dev": ensure=>installed, }
	package { "liblapack-dev": ensure=>installed, }
	package { "libxinerama1": ensure=>installed, }
	package { "libxi6": ensure=>installed, }
	package { "libxmu-dev": ensure=>installed, }	
	package { "libx11-6": ensure=>installed, }
	package { "libx11-dev": ensure=>installed, }
	package { "lsb-release": ensure=>installed, }
	package { "mesa-common-dev": ensure=>installed, }
	package { "mesa-utils": ensure=>installed, }
	package { "python-is-python3": ensure=>installed, }
	package { "python3-pip": ensure=>installed, }
	package { "r-base": ensure=>installed, }
	package { "r-base-core": ensure=>installed, }
	package { "r-base-core-dbg": ensure=>installed, }	
	package { "r-base-dev": ensure=>installed, }
	package { "r-base-html": ensure=>installed, }
	package { "sendmail": ensure=>installed, }
	package { "software-properties-common": ensure=>installed, }

	exec { 'PIP Modules': path => '/usr/bin:/bin', 
		command=>'python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose', }

	file { "Homes mount point": path => "/home/storage01", ensure => directory, owner => root, group => root, mode => "0777", }

	exec { 'Adding mount point':
		path => '/usr/bin:/bin', command =>'sed -i -e "$ a 10\.70\.8\.80:/Homes		/home/storage01	nfs	defaults,soft	0	0" /etc/fstab',
		unless => "grep storage01 /etc/fstab",
	 }

	exec { 'Mounting homes': path => '/usr/bin:/bin', command =>'mount /home/storage01', }

	file { "molden": path => "/usr/local/bin/molden", ensure => present, owner => root, group => root, mode => "0555",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/molden", }

	file { "gmolden": path => "/usr/local/bin/gmolden", ensure => present, owner => root, group => root, mode => "0555",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/gmolden", }

	file { "vmd wrapper": path => "/usr/local/bin/vmd", ensure => present, owner => root, group => root, mode => "0755",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/vmd", }

	file { "vmd sh helper": path => "/usr/local/bin/vmd.sh", ensure => present, owner => root, group => root, mode => "0755",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/vmd.sh", }

	file { "vmd csh helper": path => "/usr/local/bin/vmd.csh", ensure => present, owner => root, group => root, mode => "0755",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/vmd.csh", }

	file { "VMD Intall": path => "/tmp/vmd-1.9.3.tbz2", ensure => present, owner => root, group => root, mode => "0555",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/vmd-1.9.3.tbz2",
		replace => false,
	}

	exec { "Extract VMD": path=>"/bin:/usr/bin",
		command => "tar xjpf /tmp/vmd-1.9.3.tbz2 -C /usr/local/lib", 
		unless => "test -d /usr/local/lib/vmd",
	}


	file { "gromacs profile": path => "/etc/profile.d/gromacs.sh", ensure => present, owner => root, group => root, mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/profile.d/gromacs.sh", }

	file { "schrodinger profile": path => "/etc/profile.d/schrodinger.sh", ensure => present, owner => root, group => root, mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/profile.d/schrodinger.sh", }
	
	file { "turbomole profile": path => "/etc/profile.d/turbomole.sh", ensure => present, owner => root, group => root, mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/profile.d/turbomole.sh", }

}


class slurm_cluster {
	# In Debian 11 la versione di SLURM e' 20.11.4
	package { "slurm-wlm": ensure=>installed, }
	file { "/var/run/slurm-llnl/": path => "/var/run/slurm-llnl/", ensure => directory, owner => slurm, group => slurm, mode => "0755", }
	file { "/etc/slurm/cgroup": path => "/etc/slurm/cgroup/", ensure => directory, owner => slurm, group => slurm, mode => "0755", }
	file { "slurmctld.log": path => "/var/log/slurm/slurmctld.log", ensure => present, owner => slurm, group => slurm, mode => "0644", }
	file { "slurmd.log": path => "/var/log/slurm/slurmd.log", ensure => present, owner => slurm, group => slurm, mode => "0644", }
	file { "slurm_jobcomp.log": path => "/var/log/slurm_jobcomp.log", ensure => present, owner => slurm, group => slurm, mode => "0644", }

	file { "cgroup.conf": path => "/etc/slurm/cgroup.conf", ensure => present, owner => root, group => root, mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/conf.d/cgroup.conf", }

	file { "slurm.conf": path => "/etc/slurm/slurm.conf", ensure => present, owner => root, group => root, mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/conf.d/slurm.conf", }

	file { "munge.key": path => "/etc/munge/munge.key", ensure => present, owner => munge, group => munge, mode => "0400",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/conf.d/munge.key", }

	service { "munge": ensure=>running, }
}


class slurm_master {
	package { "firefox-esr": ensure=>installed, }
	package { "sview": ensure=>installed, }
	file { "/var/lib/slurm": path => "/var/lib/slurm/", ensure => directory, owner => slurm, group => slurm, mode => "0755", }
	file { "/var/lib/slurm/slurmctld": path => "/var/lib/slurm/slurmctld/", ensure => directory, owner => slurm, group => slurm, mode => "0755", }
	file { "clustername": path => "/var/lib/slurm/slurmctld/clustername", ensure => present, owner => slurm, group => slurm, mode => "0644", 
		content => "hpcluster", }
	file { "node_state": path => "/var/lib/slurm/slurmctld/node_state", ensure => present, owner => slurm, group => slurm, mode => "0644", }
	file { "resv_state": path => "/var/lib/slurm/slurmctld/resv_state", ensure => present, owner => slurm, group => slurm, mode => "0644", }
	file { "trigger_state": path => "/var/lib/slurm/slurmctld/trigger_state", ensure => present, owner => slurm, group => slurm, mode => "0644", }
	file { "distribConf.sh": path => "/usr/local/bin/distribConf.sh", ensure => present, owner => root, group => root, mode => "0700",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/Progs/distribConf.sh", }
}


## https://wiki.debian.org/RDMA
## http://inqbus-hosting.de/support/dokumentation/docs/debian-infiniband-howto
class infiniband {
	package { "rdma-core": ensure=>installed, }
	package { "libibverbs1": ensure=>installed, }
	package { "librdmacm1": ensure=>installed, }
	package { "libibmad5": ensure=>installed, }
	package { "libibumad3": ensure=>installed, }
	package { "ibverbs-providers": ensure=>installed, }
	package { "rdmacm-utils": ensure=>installed, }
	package { "infiniband-diags": ensure=>installed, }
	package { "libfabric1": ensure=>installed, }
	package { "ibverbs-utils": ensure=>installed, }
	package { "opensm": ensure=>installed, }
}


class docker_node {
	exec { "Get Docker key": path => '/usr/bin:/bin',
		command => 'curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg', 
		unless => 'test -e /usr/share/keyrings/docker-archive-keyring.gpg',
	}


	exec { "Set Docker REPO": path => '/usr/bin:/bin',
		command => 'echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null', 
		unless => 'test -e /etc/apt/sources.list.d/docker.list',
	}

	exec { "APT Update docker": path => '/usr/bin:/bin', command=>"apt-get update", }
	package { "docker-ce": ensure=>installed, }
	package { "docker-ce-cli": ensure=>installed, }
	package { "containerd.io": ensure=>installed, }


}


class nvidia_cuda {
	exec { "NVIDIA Keys": path => '/usr/bin:/bin',
		command=>"apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/debian11/x86_64/7fa2af80.pub", }
	exec { "NVIDIA Repo": path => '/usr/bin:/bin',
		command=>'add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/debian11/x86_64/ /"', }
	exec { "Contrib": path => '/usr/bin:/bin', command=>"add-apt-repository contrib", }
	exec { "Set NVIDIA Docker REPO": path => '/usr/bin:/bin',
		command => 'distribution=$(. /etc/os-release;echo $ID$VERSION_ID) && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | apt-key add - && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | tee /etc/apt/sources.list.d/nvidia-docker.list', 
		unless => 'test -e /etc/apt/sources.list.d/nvidia-docker.list',
}

	exec { "APT Update NVIDIA": path => '/usr/bin:/bin', command=>"apt-get update", }
	package { "cuda": ensure=>installed, }
	package { "nvidia-docker2": ensure=>installed, }
	package { "nvidia-tesla-460-driver": ensure=>installed, }
}



node gpu01.nanocospha.btbs.unimib.it {
	include standard_things
	include cospa_common
	include infiniband
	include docker_node
	include nvidia_cuda
	include slurm_master
	include slurm_cluster
	
	file { "gres.conf": path => "/etc/slurm/gres.conf", ensure => present, owner => root, group => root, mode => "0644",
		source => "https://gitlab.com/Ob1w4n/resources/-/raw/master/Nanocospha/conf.d/gres.conf", }

	service { "slurmctld": ensure=>running, }
	service { "slurmd": ensure=>running, }

	include utenza
}

node default {
	include standard_things
	include cospa_common
	include infiniband
	include slurm_cluster
	service { "slurmd": ensure=>running, }
	include utenza
}
